function Validation() 
{
    this.checkRong = function (value) {
        if(value.trim() === '')
        {
        return true;
        } 
        return false;
    };

    this.checkDoDai = function (value, minLength, maxLength) {
        if(value.length < minLength || value.length > maxLength) 
        {
            return true
        }
        return false
    };

    this.checkNum = function (value) {
        let reg = /^([0-9]+-)*([0-9]+)$/
        if (!reg.test(value)) 
        {
            return true
        }
        return false
    };

    this.checkString = function (mystring) {
        let rreg = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        if (!mystring.match(rreg)) 
        {
            return true
        }
        return false
    };

    this.checkEmail = function (myEmail) 
    {
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(!myEmail.match(re)) 
        {
            return true
        } 
        return false
    };

    this.checkPassword = function (myPassWord) 
    {
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,30}$/
        if(!myPassWord.match(reg)) 
        {
            return true
        } 
        return false
    };

    this.checkLuong = function (soLuong) {
        if (soLuong >= 1000000 && soLuong <= 20000000) 
        {
            return false
        }
        return true
    };

    this.checkGioLam = function (soGioLam) {
        if(soGioLam >= 80 && soGioLam <=200) 
        {
            return false
        }
        return true
    };

    this.checkTrungTaiKhoan = function (DSNV, id) {
        for (var i = 0; i < DSNV.length; i++) {
            if(DSNV[i].TaiKhoan === id) 
            {
                return true
            }
            return false
        }
    }
}