    // Array chúa nhân viên
    let DSNV = [];

    // validate
    let Validate = new Validation();

    //Chuc Nang 
    let ChucNang = new  xoavacapnhat();

    //  getElementByI
    function getElementById(GetEle) 
    {
    let dom = document.getElementById(GetEle)
    return dom;
    }   

    
    let jsonDanhSachNhanVien = localStorage.getItem('DanhSachNV')
    if (jsonDanhSachNhanVien != null) 
    {
            DSNV= JSON.parse(jsonDanhSachNhanVien)

            CapNhapNhanVien(DSNV)
    }
  
    // function thông báo lỗi 
    function thongBaoLoi(id, indexErr) 
    {
        return getElementById(id).innerHTML = indexErr
    }

    // thông báo err 
    let arrErr = ["Bạn chưa nhập thông tin", 
                  'Tài khoản từ 4-6 ký tự', 
                  'Tên của bạn phải là chữ', 
                  'Email không hợp lệ',
                  'Mật khẩu từ 6-10 ký tự, có chữ in hoa, số, ký tự đặc biệt', 
                  'Lương từ 1.000.000 - 20.000.000', 
                  'Số giờ làm từ 80-200 giờ',
                  'Tài khoản phải là ký số', 
                  'Tài khoản đã tồn tại']

    
    function LayThongTinTuForm() 
    {
       
        let taiKhoan = getElementById('tknv').value
        let hoTen = getElementById('name').value
        let email = getElementById('email').value
        let matKhau = getElementById('password').value
        let date = getElementById('datepicker').value
        let luongCoban = getElementById('luongCB').value
        let chucVu = getElementById('chucvu').value
        let gioLamTrongThang = getElementById('gioLam').value

        let test = 0;
        // kiem tra tai khoan
        if(checkRong('tknv', taiKhoan))  {
            thongBaoLoi('tbTKNV', arrErr[0])
            test++
        } 
        
        else if (Validate.checkNum(taiKhoan)) {
            thongBaoLoi('tbTKNV', arrErr[7])
            test++
        } 
        
        else if (Validate.checkDoDai(taiKhoan, 4, 6)) {
            test++
            thongBaoLoi('tbTKNV', arrErr[1])
        } 
        
        else if (Validate.checkTrungTaiKhoan(DSNV, taiKhoan)) {
            thongBaoLoi('tbTKNV', arrErr[8])
            test++
        } 
        
        else {
            getElementById('tknv').style.borderColor = 'green'
            thongBaoLoi('tbTKNV', '')
        }

        // kiem tra ten
        if(checkRong('name', hoTen))  {
            thongBaoLoi('tbTen', arrErr[0])
            test++
        } 
        
        else if (Validate.checkString(hoTen)) {
            test++
            thongBaoLoi('tbTen', arrErr[2])
        } 
        
        else {
            getElementById('name').style.borderColor = 'green'
            thongBaoLoi('tbTen', '')
        }

        // Kiem tra email
        if(checkRong('email', email))  {
            thongBaoLoi('tbEmail', arrErr[0])
            test++
        } 
        
        else if (Validate.checkEmail(email)) {
            thongBaoLoi('tbEmail', arrErr[3])
            test++
        }
        
        else {
            getElementById('email').style.borderColor = 'green'
            thongBaoLoi('tbEmail', '')
        }

        // kiem tra mat khau
        if(checkRong('password', matKhau))  {
            thongBaoLoi('tbMatKhau', arrErr[0])
            test++
        } 
        
        else if (Validate.checkPassword(matKhau)) {
            thongBaoLoi('tbMatKhau', arrErr[4])
            test++
        }
        
        else {
            getElementById('password').style.borderColor = 'green'
            thongBaoLoi('tbMatKhau', '')
        }


        // kiem tra date
        if(checkRong('datepicker', date)) {
            thongBaoLoi('tbNgay', arrErr[0])
            test++
        } 
        
        else {
            getElementById('datepicker').style.borderColor = 'green'
            thongBaoLoi('tbNgay', '')
        }


        // Kiem Tra Luong
        if(checkRong('luongCB', luongCoban))  {
            thongBaoLoi('tbLuongCB', arrErr[0])
            test++
        } 
        
        else if (Validate.checkLuong(luongCoban*1)) {
            thongBaoLoi('tbLuongCB', arrErr[5])
            test++
        } 
        
        else {
            getElementById('luongCB').style.borderColor = 'green'
            thongBaoLoi('tbLuongCB', '')
        }

        // Kiem Tra Gio Lam
        if(checkRong('gioLam', gioLamTrongThang))  {  
            thongBaoLoi('tbGiolam', arrErr[0])
            test++
        } 
        
        else if (Validate.checkGioLam(gioLamTrongThang)) {
            thongBaoLoi('tbGiolam', arrErr[6])
            test++
        }
        
        else {
            getElementById('gioLam').style.borderColor = 'green'
            thongBaoLoi('tbGiolam', '')
        }

        // Kiem tra Chuc vu
        if (chucVu == 0) {
            getElementById('chucvu').style.borderColor = 'red'
            thongBaoLoi('tbChucVu', arrErr[0])
            test++
        }
        
        else {
            getElementById('chucvu').style.borderColor = 'green'
            thongBaoLoi('tbChucVu', '')
        }
    
        // Add Nhan vien vao mang
        if (test != 0) {
            return 
        } 


        // gán value cho nhân viên sau khi kiểm tra dữ liệu nhập vào là true;
        let nhanVien = new NhanVien(taiKhoan, hoTen, email, matKhau, date, luongCoban, chucVu, gioLamTrongThang)

        // push cho mảng chứa nhân viên
        DSNV.push(nhanVien)

        // Them Nhan Vien
        CapNhapNhanVien(DSNV)    

        // Đẩy thông thin lên Json
        SetJson(DSNV)
    
        // gọi hàm reload browser 
        refreshPage()   

        return nhanVien
}


    // --------- Set Json --------
    function SetJson (DSNV) 
    {
        let JsonDSNV = JSON.stringify(DSNV)
        localStorage.setItem('DanhSachNV', JsonDSNV)
    }
 

    // ------ reload browser sau khi click button --------
    function refreshPage()
    {
    window.location.reload();
    } 


    // ---- validate Rong -------
    function checkRong (id, value) 
    {
        if(Validate.checkRong(value)) {
            getElementById(id).style.borderColor = 'red'
            return true;
        }
        return false
    }


    // ------------- Thêm nhân viên --------------
    function CapNhapNhanVien(arrDSNV) 
    {
        let contentHTML = '';
        let arr = arrDSNV.length
        for (var i = 0; i < arr ; i++) {
            contentHTML += `
            <tr>
                <td>${arrDSNV[i].TaiKhoan}</td>
                <td>${arrDSNV[i].HoTen}</td>
                <td>${arrDSNV[i].Email}</td>
                <td>${arrDSNV[i].Date}</td>
                <td>${arrDSNV[i].ChucVu}</td>
                <td>${arrDSNV[i].TinhLuong}</td>
                <td>${arrDSNV[i].XepLoaiNhanVien}</td>
                <td>
               
                <button onclick="XoaNhanVien(${arrDSNV[i].TaiKhoan})" class="btn btn-danger">Xóa</button>
                <button data-toggle="modal" data-target="#myModal" onclick="SuaNhanVien(${arrDSNV[i].TaiKhoan})" class="btn bg-info">Sửa</button>
                </td>
            </tr>
            `
        }
        return getElementById('tableDanhSach').innerHTML = contentHTML
    }


    // ----- function Xóa Nhân Viên -------
    function XoaNhanVien(id) {
        let ViTri = ChucNang.XoaNhanVien(id, DSNV)
        console.log(ViTri)
        if (ViTri != -1) {
            DSNV.splice(ViTri,1)
            CapNhapNhanVien(DSNV)
            SetJson(DSNV)
        }
    }

    // -------- function Lấy thông tin từ table lên form ---------
    function SuaNhanVien(id) {
        for (var i = 0; i < DSNV.length; i++) {
            if (DSNV[i].TaiKhoan*1 === id) {
                getElementById('tknv').value =  DSNV[i].TaiKhoan
                getElementById('tknv').disabled = true
                getElementById('name').value =  DSNV[i].HoTen
                getElementById('email').value =  DSNV[i].Email
                getElementById('password').value =  DSNV[i].PassWord
                getElementById('datepicker').value =  DSNV[i].Date
                getElementById('luongCB').value =  DSNV[i].LuongCoBan
                getElementById('chucvu').value =  DSNV[i].ChucVu
                getElementById('gioLam').value =  DSNV[i].GioLamTrongThang
            }

        }
    }


    //--------- function TinhLuongCapNhap ---------
    function tinhLuong(id, luongCB) {
        let tong = 0
        if(id == 'Sếp') {
            tong = luongCB *3
        } else if (id == 'Trưởng Phòng') {
            tong = luongCB *2
        } else {
            tong = luongCB*1
        }
        return tong
    }



    //------- function XepLoaiCapnhap --------
    function xepLoai(soGioLam) {
        let ketQua = ''
        if (soGioLam >=192) {
            ketQua = 'Xuất Sắc'
        } else if (soGioLam >=176) {
            ketQua = 'Giỏi'
        } else if (soGioLam >=160) {
            ketQua = 'Khá'
        } else {
            ketQua= 'Trung Bình'
        }
        return ketQua;
    }


    // Hàm cập nhập nhân viên
    function CapNhatNv() {  
        let test = 0;
        let taiKhoan = getElementById('tknv').value*1
        let viTri = ChucNang.CapNhapSinhVien(taiKhoan, DSNV)
        
        if(viTri != -1) {
            DSNV[viTri].HoTen = getElementById('name').value
            DSNV[viTri].Email = getElementById('email').value
            DSNV[viTri].PassWord = getElementById('password').value
            DSNV[viTri].Date = getElementById('datepicker').value
            DSNV[viTri].LuongCoBan = getElementById('luongCB').value
            DSNV[viTri].ChucVu = getElementById('chucvu').value
            DSNV[viTri].GioLamTrongThang = getElementById('gioLam').value
            DSNV[viTri].TinhLuong = tinhLuong(DSNV[viTri].ChucVu, DSNV[viTri].LuongCoBan)
            DSNV[viTri].XepLoaiNhanVien = xepLoai(DSNV[viTri].GioLamTrongThang)
        }   
        
        // kiem tra ten
        if(checkRong('name', DSNV[viTri].HoTen))  {
            thongBaoLoi('tbTen', arrErr[0])
            test++
        } 
        
        else if (Validate.checkString(DSNV[viTri].HoTen)) {
            test++
            thongBaoLoi('tbTen', arrErr[2])
        } 
        
        else {
            getElementById('name').style.borderColor = 'green'
            thongBaoLoi('tbTen', '')
        }

        // Kiem tra email
        if(checkRong('email', DSNV[viTri].Email))  {
            thongBaoLoi('tbEmail', arrErr[0])
            test++
        } 
        
        else if (Validate.checkEmail(DSNV[viTri].Email)) {
            thongBaoLoi('tbEmail', arrErr[3])
            test++
        }
        
        else {
            getElementById('email').style.borderColor = 'green'
            thongBaoLoi('tbEmail', '')
        }

        // kiem tra mat khau
        if(checkRong('password', DSNV[viTri].PassWord))  {
            thongBaoLoi('tbMatKhau', arrErr[0])
            test++
        } 
        
        else if (Validate.checkPassword(DSNV[viTri].PassWord)) {
            thongBaoLoi('tbMatKhau', arrErr[4])
            test++
        }
        
        else {
            getElementById('password').style.borderColor = 'green'
            thongBaoLoi('tbMatKhau', '')
        }

        // kiem tra date
        if(checkRong('datepicker', DSNV[viTri].Date)) {
            thongBaoLoi('tbNgay', arrErr[0])
            test++
        } 
        
        else {
            getElementById('datepicker').style.borderColor = 'green'
            thongBaoLoi('tbNgay', '')
        }

        // Kiem Tra Luong
        if(checkRong('luongCB', DSNV[viTri].LuongCoBan))  {
            thongBaoLoi('tbLuongCB', arrErr[0])
            test++
        } 
        
        else if (Validate.checkLuong(DSNV[viTri].LuongCoBan*1)) {
            thongBaoLoi('tbLuongCB', arrErr[5])
            test++
        } 
        
        else {
            getElementById('luongCB').style.borderColor = 'green'
            thongBaoLoi('tbLuongCB', '')
        }

        // Kiem Tra Gio Lam
        if(checkRong('gioLam', DSNV[viTri].GioLamTrongThang))  {  
            thongBaoLoi('tbGiolam', arrErr[0])
            test++
        } 
        
        else if (Validate.checkGioLam(DSNV[viTri].GioLamTrongThang)) {
            thongBaoLoi('tbGiolam', arrErr[6])
            test++
        }
        
        else {
            getElementById('gioLam').style.borderColor = 'green'
            thongBaoLoi('tbGiolam', '')
        }

        // Kiem tra Chuc vu
        if (DSNV[viTri].ChucVu == 0) {
            getElementById('chucvu').style.borderColor = 'red'
            thongBaoLoi('tbChucVu', arrErr[0])
            test++
        }
        
        else {
            getElementById('chucvu').style.borderColor = 'green'
            thongBaoLoi('tbChucVu', '')
        }
    
        // Add Nhan vien vao mang
        if (test != 0) {
            return 
        } 
        
        //RENDER nhan Vien
        CapNhapNhanVien(DSNV)

        // Đẩy thông thin lên Json
        SetJson(DSNV)
      
        // gọi hàm reload browser 
        refreshPage() 
    }

    // ------- function TimKiemNhanVien -------
    function TimKiemNhanVien() {
        //Lấy oninput user
        let tuKhoa = getElementById('searchName').value;

        //mảng chứa từ khóa
        let arrSvSearch = []

        DSNV.forEach(function (sinhVien) {
            if(sinhVien.XepLoaiNhanVien.toLowerCase().trim().search(tuKhoa.toLowerCase().trim()) != -1) 
            {
            arrSvSearch.push(sinhVien)
            }
    })  
        CapNhapNhanVien(arrSvSearch)
    }


